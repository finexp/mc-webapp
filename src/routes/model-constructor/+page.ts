import type {PageLoad} from './$types'
import {GetServer} from '$lib/srv'
import type {UpdaterDoc} from '$lib/dto'

async function GetFunctions () : Promise<UpdaterDoc[]> {
    return await( await fetch(
        `${GetServer()}/functions`,
        {method: 'get'}
    )).json();
}

export const load = (({params}) => {
    return {
        functions : GetFunctions()
    };
}) satisfies PageLoad;