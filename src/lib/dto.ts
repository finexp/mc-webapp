class HistogramAxis {
    
    state : number;
    nbins : number;
    min   : number;
    max   : number;

    constructor (state:number, nbins:number, min:number, max:number){
        this.state = state;
        this.nbins = nbins;
        this.min   = min;
        this.max   = max;
    }
}

class Histogram1D {

    x : HistogramAxis;

    constructor (x:HistogramAxis){
        this.x = x;
    }
}

class Histogram2D {

    x : HistogramAxis;
    y : HistogramAxis;

    constructor (x:HistogramAxis, y:HistogramAxis){
        this.x = x;
        this.y = y;
    }
}

class UpdaterDoc {

    name        : string;
    title       : string;
    doc_md      : string;
    start       : string;
    nargs_min   : number;
    nrefs_min   : number;

    constructor(
        name        : string,
        title       : string,
        doc_md      : string,
        start       : string,
        nargs_min   : number,
        nrefs_min   : number
    ){
        this.name      = name;
        this.title     = title;
        this.doc_md    = doc_md;
        this.start     = start;
        this.nargs_min = nargs_min;
        this.nrefs_min = nrefs_min;
    }
}

class UpdaterDto {

    name : string;
    refs : number[];
    args : number[];
    start: number|undefined|null;

    constructor (
        name  : string,
        refs  : number[] = [],
        args  : number[] = [],
        start : number|undefined|null = undefined
    ){
        this.name  = name;
        this.refs  = refs;
        this.args  = args;
        this.start = start;
    }

    GetName  = () => this.name;
    GetRefs  = () => this.refs;
    GetArgs  = () => this.args;
    GetStart = () => this.start;

    HasState() : boolean {
        return this.start!==undefined;
    }
}

class Updater extends UpdaterDto {
    _equation                       = -1;
    _state    :    number|undefined = undefined;
    constructor(
        name  : string,
        refs  : number[]         = [],
        args  : number[]         = [],
        start : number|undefined|null = undefined
    ){
        super(name,refs,args,start);
    }

    GetStateNumber() : number {
        if((typeof this._state)===undefined)
            throw new Error(`Updater ${this.name} has no state number.`);
        return this._state!;
    }
        
    GetFunctionNumber() : number {
        return this._equation;
    }
}

class Barrier extends Updater {
    constructor (
        underlying  : number,
        start       : number,
        level       : number,
        value       : number,
        direction   : number,
        action      : number
    ){
        super('Barrier',[underlying],[level, value, direction, action],start);
    }
}

class EvaluationPoint {

    state: number;
    time : number;
    histograms : (Histogram1D|Histogram2D)[];

    constructor (
        state: number,
        time : number,
        histograms : (Histogram1D|Histogram2D)[] = []
    ){
        this.state = state;
        this.time  = time;
        this.histograms = histograms;
    }

    AddHistogram(h : Histogram1D|Histogram2D) {
        this.histograms.push(h);
    }
}

class Model {

    TimeStart  : number;
    TimeSteps  : number;
    NumPaths   : number;
    updaters   : Updater[];
    evaluations: EvaluationPoint[];

    constructor (
        TimeStart  : number,
        TimeSteps  : number,
        NumPaths   : number,
        updaters   : Updater[] = [],
        evaluations: EvaluationPoint[] = []
    ){
        this.TimeStart   = TimeStart;
        this.TimeSteps   = TimeSteps;
        this.NumPaths    = NumPaths;
        this.updaters    = updaters;
        this.evaluations = evaluations;
    }

    GetNumberOfStates (): number {
        return this.updaters.filter(
            updater => updater.HasState()
        ).length;
    } 

    GetNumberOfEquations (): number {
        return this.updaters.length;
    } 

    Add (
        updater: Updater
    ): Updater {
        updater._equation = this.GetNumberOfEquations();
        updater._state    = updater.HasState() ? this.GetNumberOfStates() : undefined;
        this.updaters.push(updater);
        return updater;
    }
};

class Result {
    n        : number;
    mean     : number|undefined;
    stddev   : number|undefined;
    skewness : number|undefined;
    constructor (
        n        : number = 0,
        mean     : number|undefined = undefined,
        stddev   : number|undefined = undefined,
        skewness : number|undefined = undefined
    ){
        this.n        = n;
        this.mean     = mean;
        this.stddev   = stddev;
        this.skewness = skewness;
    }
}

class EvaluationResults {
    names       : string[] = [];
    npaths      : number[] = [];
    mean        : number[] = [];
    stddev      : number[] = [];
    skewness    : number[] = [];
    time_points : number[] = [];
    time_steps  : number[] = [];
    histograms  : any   [] = [];
    model       : Model|undefined = undefined;

    NumStates () : number {
        return this.names.length;
    }

    NumEvaluations () : number {
        return this.time_points.length;
    }

    IndexOf (
        state:number,
        point:number
    ) : number {
        if(!(
            state>=0 &&
            state<this.NumStates() &&
            point>=0 &&
            point<this.NumEvaluations()
        ))
            throw new Error('Bad state/point');

        return point*this.NumStates() + state;
    }

    GetStateEvaluationResult (
        state:number,
        point:number
    ) {
        const n = this.IndexOf(state,point);
        return new Result(
            this.npaths[n],
            this.mean[n],
            this.stddev[n],
            this.skewness[n]
        );
    }
}

function GetResultIndexOf (
    results:EvaluationResults,
    state:number,
    point:number
) : number {
    return point*results.names.length + state;
}

function GetResultTime (
    results:EvaluationResults,
    state:number,
    point:number
) : number {
    return results.time_points[GetResultIndexOf(results,state,point)];
}

function GetResultTimeStep (
    results:EvaluationResults,
    state:number,
    point:number
) : number {
    return results.time_steps[GetResultIndexOf(results,state,point)];
}

function GetResultNpaths (
    results:EvaluationResults,
    state:number,
    point:number
) : number {
    return results.npaths[GetResultIndexOf(results,state,point)];
}

function GetResultMean (
    results:EvaluationResults,
    state:number,
    point:number
) : number {
    return results.mean[GetResultIndexOf(results,state,point)];
}

function GetResultMeanError (
    results:EvaluationResults,
    state:number,
    point:number
) : number {
    const
        i       = GetResultIndexOf(results,state,point),
        npaths  = results.npaths[i],
        stddev  = results.stddev[i];
    return stddev/Math.sqrt(npaths);
}

function GetResultStddev (
    results:EvaluationResults,
    state:number,
    point:number
) : number {
    return results.stddev[GetResultIndexOf(results,state,point)];
}

function GetResultSkewness (
    results:EvaluationResults,
    state:number,
    point:number
) : number {
    return results.skewness[GetResultIndexOf(results,state,point)];
}

export {
    UpdaterDoc,
    UpdaterDto,
    Updater,
    Barrier,
    EvaluationPoint,
    Model,
    Result,
    EvaluationResults,
    GetResultIndexOf,
    GetResultTime,
    GetResultTimeStep,
    GetResultNpaths,
    GetResultMean,
    GetResultMeanError,
    GetResultStddev,
    GetResultSkewness,
    Histogram1D,
    Histogram2D,
    HistogramAxis
}
