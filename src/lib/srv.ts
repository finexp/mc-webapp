import {Model, EvaluationResults} from './dto'

function GetServer (): string {
    return 'http://naz.hopto.org:8001';
}

async function RunModel (
    modelStructure: Model,
    server        : string
): Promise<EvaluationResults> {
    return await( await fetch(
        `${server}/model`,
        {
            method: 'post',
            body: JSON.stringify(modelStructure)
        }
    )).json();
}

export {GetServer, RunModel}